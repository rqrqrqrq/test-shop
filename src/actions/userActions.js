import * as types from '../constaints/actionTypes';

export const logIn = data => ({
  type: types.USER_LOGGED_IN,
  payload: data
})

export const logOut = () => dispatch => {
  // HACK
  window.FB.logout();
  
  dispatch({
    type: types.USER_LOGGED_OUT,
  })
}
