import * as types from '../constaints/actionTypes';
import { fetchItems } from '../api';

export const getItems = () => dispatch => {
  dispatch({
    type: types.FETCH_ITEMS_STARTED
  })
  fetchItems()
    .then(response => dispatch({
      type: types.FETCH_ITEMS_SUCCESS,
      payload: response.data
    }))
    .catch(error => dispatch({
      type: types.FETCH_ITEMS_FAILED,
      payload: error
    }));
}
