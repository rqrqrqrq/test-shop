import * as types from '../constaints/actionTypes';

export const addToCartById = itemId => ({
  type: types.ADD_TO_CART,
  payload: itemId
})

export const removeFromCartById = itemId => ({
  type: types.REMOVE_FROM_CART,
  payload: itemId
})
