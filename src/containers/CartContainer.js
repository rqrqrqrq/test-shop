import Cart from '../components/Cart';
import { connect } from 'react-redux';
import { removeFromCartById } from '../actions/cartActions';

const mapStateToProps = ({ cart, items }) => ({
  itemList: Object.keys(cart).map(key => ({
    ...items.find(item => item.id === key),
    count: cart[key]
  }))
});

const mapDispatchToProps = dispatch => ({
  removeFromCart: id => dispatch(removeFromCartById(id))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Cart);
