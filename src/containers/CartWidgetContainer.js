import { connect } from 'react-redux';
import CartWidget from '../components/CartWidget';

const calcItemsCount = cart => {
  return Object.keys(cart).reduce((count, key) => {
    return count += cart[key];
  }, 0)
}

const calcTotalPrice = (cart, itemList) => {
  return Object.keys(cart).reduce((totalPrice, key) => {
    const itemPrice = itemList.find(item => item.id === key).price;
    return totalPrice += itemPrice * cart[key];
  }, 0)
}

const mapStateToProps = ({ cart, items }) => ({
  count: calcItemsCount(cart),
  total: calcTotalPrice(cart, items)
})

export default connect(mapStateToProps)(CartWidget);
