import { connect } from 'react-redux';
import UserWidget from '../components/UserWidget';
import { logOut } from '../actions/userActions';
import { isUserAuthed } from '../auth';

const mapStateToProps = ({ user }) => ({
  user,
  isAuthed: isUserAuthed(user)
});

const mapDispatchToProps = dispatch => ({
  handleSignOut: () => dispatch(logOut())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserWidget);
