import React from 'react';
import { connect } from 'react-redux';
import FacebookLogin from 'react-facebook-login';
import { logIn } from '../actions/userActions';
import { FACEBOOK_APP_ID } from '../constaints/apiConstaints';

const Login = ({ responseFacebook }) =>
  <FacebookLogin
    appId={FACEBOOK_APP_ID}
    autoLoad={true}
    fields="name,picture"
    callback={responseFacebook}
  />

const mapDispatchToProps = dispatch => ({
  responseFacebook: data => dispatch(logIn(data))
});

export default connect(
  null,
  mapDispatchToProps
)(Login);
