import React from 'react';
import { connect } from 'react-redux';

const Spinner = ({ isFetching }) =>
  isFetching ?
    <div style={{
      display: 'flex',
      backgroundColor: 'rgba(255, 255, 255, 0.5)',
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      zIndex: 100
    }}>
      <span style={{
        margin: 'auto',
        color: 'red',
        fontSize: 40,
        fontWeight: 'bold'
      }}>
        SPINNER
      </span>
    </div>
    : null;

const mapStateToProps = (state) => ({
  isFetching: state.status.isFetching
});

export default connect(mapStateToProps)(Spinner);
