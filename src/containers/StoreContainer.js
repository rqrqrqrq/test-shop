import { connect } from 'react-redux';
import Store from '../components/Store';
import { getItems } from '../actions/storeActions';
import { addToCartById } from '../actions/cartActions';

const mapStateToProps = ({ items }) => ({
  itemList: items
});

const mapDispatchToProps = dispatch => ({
  getItems: () => dispatch(getItems()),
  addToCart: id => dispatch(addToCartById(id))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Store);
