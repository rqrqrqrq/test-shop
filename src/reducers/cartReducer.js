import * as types from '../constaints/actionTypes';

const initialState = {};

const cartReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.ADD_TO_CART:
      let count = state[payload];
      return {
        ...state,
        [payload]: count ? count + 1 : 1
      };

    case types.REMOVE_FROM_CART:
      const newCount = state[payload] - 1;
      if (newCount === 0) {
        const newState = { ...state };
        delete newState[payload];
        return newState;
      } else return {
        ...state,
        [payload]: newCount
      };

    case types.USER_LOGGED_OUT:
      return initialState;

    default:
      return state;
  }
}

export default cartReducer;
