import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import userReducer from './userReducer';
import cartReducer from './cartReducer';
import itemsReducer from './itemsReducer';
import statusReducer from './statusReducer';

export default combineReducers({
  routing: routerReducer,
  user: userReducer,
  cart: cartReducer,
  items: itemsReducer,
  status: statusReducer
});
