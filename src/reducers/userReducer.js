import * as types from '../constaints/actionTypes';

const initialState = {};

const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.USER_LOGGED_IN:
      return payload;

    case types.USER_LOGGED_OUT:
      return initialState;

    default:
      return state;
  }
}

export default userReducer;
