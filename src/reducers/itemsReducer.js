import * as types from '../constaints/actionTypes';

const itemsReducer = (state = [], { type, payload }) => {
  switch (type) {
    case types.FETCH_ITEMS_SUCCESS:
      return payload;
    default:
      return state;
  }
}

export default itemsReducer;
