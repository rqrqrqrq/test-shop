import * as types from '../constaints/actionTypes';

const initialState = {
  isFetching: false,
  errors: null
};

const statusReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.FETCH_ITEMS_STARTED:
      return { ...state, isFetching: true };

    case types.FETCH_ITEMS_SUCCESS:
      return { ...state, isFetching: false };

    case types.FETCH_ITEMS_FAILED:
      return {
        ...state,
        isFetching: false,
        errors: state.errors ? [ ...state.errors, payload ] : null
      };

    default:
      return state;
  }
}

export default statusReducer;
