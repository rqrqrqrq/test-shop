import { UserAuthWrapper } from 'redux-auth-wrapper';
import { routerActions } from 'react-router-redux';

export const isUserAuthed = user => Boolean(user && user.userID);

export const UserIsAuthenticated = UserAuthWrapper({
  authSelector: state => state.user,
  redirectAction: routerActions.redirect,
  predicate: user => isUserAuthed(user)
});

export const UserIsNotAuthenticated = UserAuthWrapper({
  authSelector: state => state.user,
  redirectAction: routerActions.redirect,
  predicate: user => !isUserAuthed(user),
  failureRedirectPath: (state, ownProps) => ownProps.location.query.redirect || '/',
  allowRedirectBack: false
});

export const AuthRequiredWrapper = UserIsAuthenticated(props => props.children);
