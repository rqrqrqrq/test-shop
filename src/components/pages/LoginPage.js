import React from 'react';
import Login from '../../containers/Login';

const LoginPage = () =>
  <div>
    <h1>Login Page</h1>
    <Login />
  </div>

export default LoginPage;
