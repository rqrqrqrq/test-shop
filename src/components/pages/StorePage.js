import React from 'react';
import Store from '../../containers/StoreContainer';

const StorePage = () =>
  <div>
    <h1>Store Page</h1>
    <Store />
  </div>;

export default StorePage;
