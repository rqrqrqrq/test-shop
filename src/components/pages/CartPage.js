import React from 'react';
import Cart from '../../containers/CartContainer';

const CartPage = () =>
  <div>
    <h1>Cart Page</h1>
    <Cart />
  </div>

export default CartPage;
