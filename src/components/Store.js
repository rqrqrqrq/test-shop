import React, { Component } from 'react';

class Store extends Component {
  componentDidMount() {
    this.props.getItems();
  }

  render() {
    const { itemList, addToCart } = this.props;
    return (
      <ul>
        {itemList.map(item => (
          <li key={item.id}>
            {`Name: ${item.name} :: Price: $${item.price} :: `}
            <button onClick={() => addToCart(item.id)}>
              Add to cart
            </button>
          </li>
        ))}
      </ul>
    )
  }
}

export default Store;
