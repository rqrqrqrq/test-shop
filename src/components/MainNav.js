import React from 'react';
import { Link, IndexLink } from 'react-router';

const activeStyle = {
  color: 'red'
};

const MainNav = () =>
  <nav>
    <li>
      <IndexLink
        to="/"
        activeStyle={activeStyle}
      >
        Home
      </IndexLink>
    </li>
    <li>
      <Link
        to="store"
        activeStyle={activeStyle}
      >
        Store
      </Link>
    </li>
  </nav>

export default MainNav;
