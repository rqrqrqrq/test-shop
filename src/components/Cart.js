import React from 'react';

const Cart = ({ itemList, removeFromCart }) =>
  <ul>
    {
      itemList.length === 0 ? 'Empty' :
        itemList.map(item =>
          <li key={item.id}>
            {`Name: ${item.name} :: Price: $${item.price} :: \
            Count: ${item.count} :: TotalPrice: ${item.price * item.count} :: `}
            <button onClick={() => removeFromCart(item.id)}>
              Remove
            </button>
          </li>
        )
    }
  </ul>

export default Cart;
