import React from 'react';
import MainNav from './MainNav';
import UserWidget from '../containers/UserWidgetContainer';
import CartWidget from '../containers/CartWidgetContainer';

const Header = () =>
  <header style={{
    display: 'flex',
    justifyContent: 'space-between',
    maxWidth: 960,
    margin: '0 auto',
    padding: '10px 15px'
  }}>
    <MainNav />
    <CartWidget />
    <UserWidget />
  </header>

export default Header;
