import React from 'react';
import { Link } from 'react-router';

const CartWidget = ({ count, total }) =>
  <div>
    <Link
      to="cart"
      activeStyle={{ color: 'red' }}
    >
      Cart
    </Link>
    <br />
    {count !== 0 && `items: ${count}
    `}
    {total !== 0 && `total: $${total}`}
  </div>

export default CartWidget;
