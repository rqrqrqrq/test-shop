import React from 'react';
import UserProfile from './UserProfile';
import { Link } from 'react-router';

const UserWidget = ({ user, isAuthed, handleSignOut }) =>
  <div>
    {isAuthed
      ? <UserProfile
        user={user}
        handleSignOut={handleSignOut}
      />
      : <Link
        to="login"
        activeStyle={{ color: 'red' }}
      >
        Sign In
      </Link>
    }
  </div>

export default UserWidget;
