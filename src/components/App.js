import React from 'react';
import Header from './Header';
import Spinner from '../containers/Spinner';


const App = ({ children }) =>
  <div>
    <Header />
    <main style={{
      margin: '0 auto',
      maxWidth: 960,
      padding: '10px 15px'
    }}>
      {children}
    </main>
    <Spinner />
  </div>;

export default App;
