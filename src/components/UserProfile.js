import React from 'react';

const UserProfile = ({ user, handleSignOut }) =>
  <span>
    <img src={user.picture.data.url} alt={user.name} />
    {user.name}
    <br />
    <a href="#" onClick={handleSignOut}>Sign Out</a>
  </span>

export default UserProfile;
