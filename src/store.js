import { createStore, applyMiddleware } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import { composeWithDevTools } from 'remote-redux-devtools';
import thunkMiddleware from 'redux-thunk';
import rootReducer from './reducers/rootReducer';

const configureStore = history => createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(
    thunkMiddleware,
    routerMiddleware(history)
  ))
);

export default configureStore;
