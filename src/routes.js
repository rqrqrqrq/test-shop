import React from 'react';
import { Route, IndexRoute } from 'react-router';
import { UserIsNotAuthenticated } from './auth';
import { AuthRequiredWrapper } from './auth';

import App from './components/App';
import LoginPage from './components/pages/LoginPage';
import HomePage from './components/pages/HomePage';
import StorePage from './components/pages/StorePage';
import CartPage from './components/pages/CartPage';

export default (
  <Route path="/" component={App}>
    <Route path="login" component={UserIsNotAuthenticated(LoginPage)} />
    <IndexRoute component={HomePage} />
    <Route component={AuthRequiredWrapper}>
      <Route path="store" component={StorePage} />
      <Route path="cart" component={CartPage} />
    </Route>
  </Route>
);
