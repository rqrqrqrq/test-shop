import Axios from 'axios';
import { API_URL } from './constaints/apiConstaints';

export const fetchItems = () => Axios.get(`${API_URL}/items`);
